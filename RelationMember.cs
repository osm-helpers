/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.Collections.Specialized;

namespace OpenStreetMap {

	public sealed class RelationMember {
		readonly Relation parent;
		string role;
		string type;
		long refid;

		public RelationMember (Relation rel, string type, string role, long refid)
		{
			parent = rel;
			this.type = type;
			this.role = role;
			this.refid = refid;
		}

		public long Ref {
			get {
				return refid;
			}
			set {
				refid = value;
				parent.Changed = true;
			}
		}

		public string Role {
			get {
				return role;
			}
			set {
				role = value;
				parent.Changed = true;
			}
		}

		public string Type {
			get {
				return type;
			}
			set {
				type = value;
				parent.Changed = true;
			}
		}

	}
}


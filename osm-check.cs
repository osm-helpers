/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;
using System.Collections.Generic;
using OpenStreetMap;

class OsmCheck {

	static string[] way_types = new string[] {};

/*
	static string[] allowed_lowercase = new string[] {
		"da", "di", "de", "del", "dei", "delle", "degli", "dell", "della", "dello",
		"sul", "sull", "d", "al", "a", "in", "e"
	};
*/

	static string[] abbreviations = new string[] {};

	static Dictionary<string,string> wrong_tags_in_nodes = new Dictionary<string,string> ();
	static Dictionary<string,string> wrong_tags_in_ways = new Dictionary<string,string> ();

	static void LoadRules (string filename)
	{
		if (filename == null)
			filename = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), "osm-helpers/osm-check.rules");
		XmlTextReader reader = new XmlTextReader (filename);
		List<string> abbrs = new List<string> ();
		List<string> ways = new List<string> ();

		while (reader.Read ()) {
			if (reader.NodeType == XmlNodeType.Element) {
				string name = reader.Name;

				if (name == "osmrules") {
					continue;
				}
				if (name == "abbr") {
					string from = null;
					string to = null;
					for (int i = 0; i < reader.AttributeCount; ++i) {
						reader.MoveToAttribute (i);
						if (reader.Name == "from")
							from = reader.Value;
						else if (reader.Name == "to")
							to = reader.Value;
						else
							throw new Exception ("invalid attribute in abbr element");
					}
					if (from == null || to == null)
						throw new Exception ("missing attributes in abbr element");
					abbrs.Add (from);
					abbrs.Add (to);
				} else if (name == "way") {
					string n = null;
					for (int i = 0; i < reader.AttributeCount; ++i) {
						reader.MoveToAttribute (i);
						if (reader.Name == "name")
							n = reader.Value;
						else
							throw new Exception ("invalid attribute in way element");
					}
					if (n == null)
						throw new Exception ("missing name attribute in way element");
					ways.Add (n);
				} else if (name == "remove") {
					string type = null;
					string tag = null;
					string val = "";
					for (int i = 0; i < reader.AttributeCount; ++i) {
						reader.MoveToAttribute (i);
						if (reader.Name == "type")
							type = reader.Value;
						else if (reader.Name == "tag")
							tag = reader.Value;
						else if (reader.Name == "val")
							val = reader.Value;
						else
							throw new Exception ("invalid attribute in remove element");
					}
					if (type == null || tag == null)
						throw new Exception ("missing type or tag attribute in remove element");
					if (type == "way")
						wrong_tags_in_ways [tag] = val;
					else if (type == "node")
						wrong_tags_in_nodes [tag] = val;
					else
						throw new Exception ("invalid type attribute in remove element");
				} else {
					throw new Exception ("invalid element");
				}
			}
		}
		abbreviations = abbrs.ToArray ();
		way_types = ways.ToArray ();
	}

	static XmlTextWriter writer;

	static void EmitElement (OsmObject obj, string tag, string old, string newv)
	{
		writer.WriteStartElement (obj.ObjectType);
		writer.WriteAttributeString ("id", obj.ID.ToString ());
		writer.WriteAttributeString ("tag", tag);
		writer.WriteAttributeString ("old", old);
		writer.WriteAttributeString ("new", newv);
		writer.WriteAttributeString ("user", obj.User);
		writer.WriteEndElement ();
	}

	static string TrimIfNeeded (string tag, string val)
	{
		// we avoid changing note, as this makes it easier to track down
		// who actually wrote the note and ask them about it
		if (tag == "note")
			return val;
		if (char.IsWhiteSpace (val [0]) || char.IsWhiteSpace (val [val.Length - 1]))
			val = val.Trim ();
		for (int i = 0; i < val.Length; ++i) {
			if (char.IsWhiteSpace (val [i]) && char.IsWhiteSpace (val [i + 1])) {
				// good enough for the use case
				val = val.Remove (i + 1, 1);
				i--;
			}
		}
		return val;
	}

	static string CheckStartWord (string tag, string val)
	{
		if (tag == "note")
			return val;
		foreach (string s in way_types) {
			if (!val.StartsWith (s) && val.StartsWith (s, StringComparison.OrdinalIgnoreCase)) {
				if (val.Length > s.Length && !char.IsWhiteSpace (val [s.Length]))
					continue;
				string newv = s + val.Substring (s.Length);
				return newv;
			}
		}
		return val;
	}

	static string CheckAbbreviations (string val)
	{
		for (int i = 0; i < abbreviations.Length; i += 2) {
			string s = abbreviations [i];
			// consider the abbreviation in any case variant
			if (val.StartsWith (s, StringComparison.OrdinalIgnoreCase)) {
				if (val.Length > s.Length && !char.IsWhiteSpace (val [s.Length]))
					continue;
				string newv = abbreviations [i + 1] + val.Substring (s.Length);
				return newv;
			}
		}
		return val;
	}

	static string CheckAllowedTags (OsmObject obj, string tag, string val)
	{
		if (obj is Node) {
			if (wrong_tags_in_nodes.ContainsKey (tag)) {
				string n = wrong_tags_in_nodes [tag];
				if (n == "" || n == val)
					return "";
				return val;
			}
		} else if (obj is Way) {
			if (wrong_tags_in_ways.ContainsKey (tag)) {
				string n = wrong_tags_in_ways [tag];
				if (n == "" || n == val)
					return "";
				return val;
			}
		}
		return val;
	}

/*
	static string CheckAllowedLowercase (OsmObject obj, string tag, string val)
	{
		// just for names, but we may want to consider only highways as well
		if (tag != "name" || !(obj is Way))
			return val;
		if (obj ["highway"] == null)
			return val;
		for (int i = 0; i < val.Length; ++i) {
			if (char.IsLower (val [i])) {
				int j = i + 1;
				while (j < val.Length && (!char.IsWhiteSpace (val [j]) && val [j] != '\''))
					j++;
				int count = j - i;
				string word = val.Substring (i, count);
				bool found = false;
				foreach (string allowed in allowed_lowercase) {
					if (string.CompareOrdinal (word, allowed) == 0) {
						found = true;
						break;
					}
				}
				if (!found) {
					StringBuilder sb = new StringBuilder (val);
					sb [i] = char.ToUpper (val [i]);
					Console.WriteLine ("{0} changed to {1}", val, sb.ToString ());
				}
				i = j - 1;
				continue;
			}
			while (i < val.Length && (!char.IsWhiteSpace (val [i]) && val [i] != '\''))
				i++;
			while (i < val.Length && (char.IsWhiteSpace (val [i]) || val [i] == '\''))
				i++;
			i--;
		}
		return val;
	}
*/

	static void Usage ()
	{
		Usage_r (1);
	}

	static void Usage_r (int retval)
	{
		Console.WriteLine ("Usage: osm-check [OPTIONS] file.osm");
		Console.WriteLine ("       osm-check [OPTIONS] lat lon [delta]");
		Console.WriteLine ("       osm-check --help");
		Console.WriteLine ("Options:");
		Console.WriteLine ("\t--help               Print the help and exit");
		Console.WriteLine ("\t--rules rules_file   Read the checking rules from rules_file");
		Console.WriteLine ("\t--out out_file       Output the changes to out_file (default: stdout)");
		Environment.Exit (retval);
	}

	static int Main (string[] args)
	{
		string rules = null;
		string file = null;
		string outfile = null;
		bool coords = false;
		double delta = 0.02;
		double lat = 0;
		double lon = 0;

		for (int i = 0; i < args.Length; ++i) {
			if (args [i] == "--help") {
				Usage_r (0);
			} else if (args [i] == "--rules") {
				if (++i >= args.Length)
					Usage ();
				rules = args [i];
			} else if (args [i] == "--out") {
				if (++i >= args.Length)
					Usage ();
				outfile = args [i];
			} else {
				switch (args.Length - i) {
				case 1:
					file = args [i];
					break;
				case 3:
					delta = double.Parse (args [i + 2]);
					goto case 2;
				case 2:
					lat = double.Parse (args [i + 0]);
					lon = double.Parse (args [i + 1]);
					coords = true;
					break;
				default:
					Usage ();
					break;
				}
			}
		}
		if (file == null && !coords)
			Usage ();
		try {
			LoadRules (rules);
		} catch (Exception e) {
			Console.WriteLine ("Cannot load rules: {0}", e.Message);
			return 2;
		}
	
		DataBase db = new DataBase ();
		IEnumerable<OsmObject> list;
		if (file != null) {
			list = DataBase.LoadObjects (file);
		} else {
			list = db.Load (lon-delta, lat-delta, lon+delta, lat+delta);
		}
		Stream outstream;
		if (outfile == null)
			outstream = Console.OpenStandardOutput ();
		else
			outstream = File.Create (outfile);
		writer = new XmlTextWriter (outstream, new UTF8Encoding (false, false));
		writer.WriteStartDocument ();
		writer.WriteStartElement ("osmchanges");
		writer.Formatting = Formatting.Indented;
		foreach (OsmObject obj in list) {
			foreach (string tag in obj.Tags) {
				string val = obj [tag];
				if (val == null || val.Length == 0)
					continue;
				string newv = TrimIfNeeded (tag, val);
				if ((object)val != (object)newv) {
					EmitElement (obj, tag, val, newv);
					continue;
				}
				newv = CheckStartWord (tag, val);
				if ((object)val != (object)newv) {
					EmitElement (obj, tag, val, newv);
					continue;
				}
				newv = CheckAbbreviations (val);
				if ((object)val != (object)newv) {
					EmitElement (obj, tag, val, newv);
					continue;
				}
				newv = CheckAllowedTags (obj, tag, val);
				if ((object)val != (object)newv) {
					EmitElement (obj, tag, val, newv);
					continue;
				}
				/*
				 * too much noise for now
				newv = CheckAllowedLowercase (obj, tag, val);
				if ((object)val != (object)newv) {
					EmitElement (obj, tag, val, newv);
					continue;
				}*/
			}
		}
		writer.WriteEndElement ();
		writer.WriteEndDocument ();
		writer.Close ();
		if (outfile != null)
			outstream.Close ();
		return 0;
	}
}


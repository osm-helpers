/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;
using System.Collections.Generic;

namespace OpenStreetMap {

	public class DataBase {

		static readonly string BaseUrl = "http://api.openstreetmap.org/api/0.5/";
		string url;

		public DataBase () : this (BaseUrl) {
		}

		public DataBase (string base_url) {
			url = base_url;
		}

		static bool ParseBool (string v)
		{
			if (v == "true")
				return true;
			if (v == "false")
				return false;
			throw new Exception ("Invalid boolean values: " + v);
		}

		public static List<OsmObject> Load (string filename)
		{
			return Load (File.OpenRead (filename));
		}

		public static List<OsmObject> Load (Stream stream)
		{
			List<OsmObject> list = new List<OsmObject> ();
			foreach (OsmObject obj in LoadObjects (stream))
				list.Add (obj);
			return list;
		}

		public static IEnumerable<OsmObject> LoadObjects (string filename)
		{
			return LoadObjects (File.OpenRead (filename));
		}

		public static IEnumerable<OsmObject> LoadObjects (Stream stream)
		{
			//XmlTextReader reader = new XmlTextReader (stream);
			XmlTextReader reader = XmlTextReader.Create (stream) as XmlTextReader;
			Way way = null;
			Node node = null;
			Relation relation = null;
			OsmObject bobject = null;
			List<long> nodelist = null;
			List<RelationMember> members = null;
			bool do_yield = false;

			while (reader.Read ()) {
				if (reader.NodeType == XmlNodeType.Element) {
					string name = reader.Name;
					if (name == "node") {
						bobject = node = new Node ();
						do_yield = reader.IsEmptyElement;
					} else if (name == "way") {
						bobject = way = new Way ();
						do_yield = reader.IsEmptyElement;
					} else if (name == "relation") {
						bobject = relation = new Relation ();
						do_yield = reader.IsEmptyElement;
					} else if (name == "osm" || name == "bounds") {
						continue;
					} else if (name == "member") {
						long refid = -1;
						string role = null;
						string type = null;
						if (members == null)
							members = new List<RelationMember> ();
						for (int i = 0; i < reader.AttributeCount; ++i) {
							reader.MoveToAttribute (i);
							if (reader.Name == "ref")
								refid = long.Parse (reader.Value);
							else if (reader.Name == "role")
								role = reader.Value;
							else if (reader.Name == "type")
								type = reader.Value;
							else
								throw new Exception ("Invalid member attribute: " + reader.Name);
						}
						members.Add (new RelationMember (relation, type, role, refid));
						continue;
					} else if (name == "nd") {
						if (nodelist == null)
							nodelist = new List<long> ();
						for (int i = 0; i < reader.AttributeCount; ++i) {
							reader.MoveToAttribute (i);
							if (reader.Name == "ref")
								nodelist.Add (long.Parse (reader.Value));
							else
								throw new Exception ("Invalid nd attribute: " + reader.Name);
						}
						continue;
					} else if (name == "tag") {
						string k = null, v = null;
						for (int i = 0; i < reader.AttributeCount; ++i) {
							reader.MoveToAttribute (i);
							if (reader.Name == "k")
								k = reader.Value;
							else if (reader.Name == "v")
								v = reader.Value;
							else
								throw new Exception ("Invalid tag attribute: " + reader.Name);
						}
						if (k == null | v == null)
							throw new Exception ("Invalid tag");
						bobject [k] = v;
						continue;
					} else {
						//Console.WriteLine ("element: {0}", name);
						continue;
					}
					for (int i = 0; i < reader.AttributeCount; ++i) {
						reader.MoveToAttribute (i);
						name = reader.Name;
						if (name == "id") {
							bobject.ID = long.Parse (reader.Value);
						} else if (name == "user") {
							bobject.User = reader.Value;
						} else if (name == "timestamp") {
							bobject.TimeStamp = reader.Value;
						} else if (name == "visible") {
							bobject.Visible = ParseBool (reader.Value);
						} else if (name == "lon") {
							node.Longitude = double.Parse (reader.Value);
						} else if (name == "lat") {
							node.Latitude = double.Parse (reader.Value);
						}
					}

				} else if (reader.NodeType == XmlNodeType.EndElement) {
					string name = reader.Name;
					if (name == "way" || name == "node" || name == "relation")
						do_yield = true;
				}
				if (do_yield) {
					if (bobject != null) {
						do_yield = reader.IsEmptyElement;
						if (bobject is Way && nodelist != null) {
							way.Nodes = nodelist.ToArray ();
							nodelist = null;
						} else if (bobject is Relation && members != null) {
							relation.Members = members.ToArray ();
							members = null;
						}
						yield return bobject;
						bobject = null;
					}
					do_yield = false;
				}
			}

		}

		public OsmObject LoadObject (string type, long id)
		{
			HttpWebRequest req = WebRequest.Create (string.Format (url + "{0}/{1}", type, id)) as HttpWebRequest;
			req.Timeout = 5000;
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			//Console.WriteLine ("{0}: {1}", res.StatusCode, res.StatusDescription);
			Stream s = res.GetResponseStream ();
			List<OsmObject> list = Load (s);
			s.Close ();
			return list [0];
		}

		public OsmObject LoadWay (long id)
		{
			return LoadObject ("way", id);
		}

		public OsmObject LoadNode (long id)
		{
			return LoadObject ("node", id);
		}

		public OsmObject LoadRelation (long id)
		{
			return LoadObject ("relation", id);
		}

		public IEnumerable<OsmObject> LoadHistory (string type, long id)
		{
			HttpWebRequest req = WebRequest.Create (string.Format (url + "{0}/{1}/history", type, id)) as HttpWebRequest;
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			//Console.WriteLine ("{0}: {1}", res.StatusCode, res.StatusDescription);
			Stream s = res.GetResponseStream ();
			List<OsmObject> list = Load (s);
			s.Close ();
			return list;
		}

		public List<OsmObject> Load (double lon1, double lat1, double lon2, double lat2)
		{
			HttpWebRequest req = WebRequest.Create (string.Format (url + "map?bbox={0},{1},{2},{3}", lon1, lat1, lon2, lat2)) as HttpWebRequest;
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			//Console.WriteLine ("{0}: {1}", res.StatusCode, res.StatusDescription);
			Stream s = res.GetResponseStream ();
			List<OsmObject> list = Load (s);
			s.Close ();
			return list;
		}

		public bool UpLoad (OsmObject obj, string username, string password)
		{
			if (obj is Relation)
				return false;
			//ServicePointManager.Expect100Continue = false;
			CredentialCache creds = new CredentialCache ();
			creds.Add (new Uri (url), "Basic", new NetworkCredential (username, password));
			HttpWebRequest req = WebRequest.Create (string.Format ("{0}{1}/{2}", url, obj.ObjectType, obj.ID)) as HttpWebRequest;
			req.Method = "PUT";
			req.Credentials = creds;
			// stupid apps don't support Expect and it's used by default
			req.ServicePoint.Expect100Continue = false;
			req.ReadWriteTimeout = 5000;
			req.Timeout = 5000;
			Stream s = req.GetRequestStream ();
			XmlTextWriter xmlw = new XmlTextWriter (s, new UTF8Encoding (false, false));
			xmlw.Formatting = Formatting.Indented;
			xmlw.WriteStartDocument ();
			xmlw.WriteStartElement ("osm");
			xmlw.WriteAttributeString ("version", "0.5");
			xmlw.WriteAttributeString ("creator", "osm-lupus-tools 0.1");
			obj.WriteXml (xmlw);
			xmlw.WriteEndElement ();
			xmlw.WriteEndDocument ();
			xmlw.Close ();
			s.Close ();
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			// need to get the stream and close it to avoid deadlocks
			s = res.GetResponseStream ();
			s.Close ();
			//Console.WriteLine ("{0}: {1}", res.StatusCode, res.StatusDescription);
			return false;
		}
	}
}


/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.Xml;
using System.Collections.Specialized;

namespace OpenStreetMap {

	public class Relation: OsmObject {
		RelationMember[] members;

		public override string ObjectType {
			get {
				return "relation";
			}
		}

		public RelationMember[] Members {
			get {
				if (members == null)
					return new RelationMember [0];
				return members;
			}
			set {
				if (value == null)
					throw new ArgumentNullException ("value");
				members = value;
				Changed = true;
			}
		}

		public override void WriteXml (XmlTextWriter writer)
		{
			writer.WriteStartElement ("relation");
			WriteCommonAttrs (writer);
			if (members != null) {
				for (int i = 0; i < members.Length; ++i) {
					writer.WriteStartElement ("member");
					writer.WriteAttributeString ("type", members [i].Type);
					writer.WriteAttributeString ("ref", members [i].Ref.ToString ());
					writer.WriteAttributeString ("role", members [i].Role);
					writer.WriteEndElement ();
				}
			}
			WriteTags (writer);
			writer.WriteEndElement ();
		}
	}
}


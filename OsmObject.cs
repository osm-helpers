/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Collections.Specialized;

namespace OpenStreetMap {

	public abstract class OsmObject {
		StringDictionary tags;
		string user;
		string timestamp;
		long id;
		bool visible;
		bool changed;

		public abstract string ObjectType {
			get;
		}

		public virtual void WriteXml (XmlTextWriter writer)
		{
		}

		public long ID {
			get {
				return id;
			}
			set {
				id = value;
				changed = true;
			}
		}

		public bool Visible {
			get {
				return visible;
			}
			set {
				visible = value;
				changed = true;
			}
		}

		public bool Changed {
			get {
				return changed;
			}
			set {
				changed = value;
			}
		}

		public string User {
			get {
				return user;
			}
			set {
				user = value;
				changed = true;
			}
		}

		public string TimeStamp {
			get {
				return timestamp;
			}
			set {
				timestamp = value;
				changed = true;
			}
		}

		public string this [string key] {
			get {
				if (tags == null)
					return null;
				return tags [key];
			}
			set {
				if (tags == null)
					tags = new StringDictionary ();
				tags [key] = value;
				changed = true;
			}
		}

		public void RemoveTag (string tag)
		{
			if (tags == null)
				return;
			tags.Remove (tag);
		}

		public ICollection Tags {
			get {
				if (tags == null)
					return new string [0];
				return tags.Keys;
			}
		}

		public void WriteCommonAttrs (XmlTextWriter writer)
		{
			writer.WriteAttributeString ("id", ID.ToString ());
			writer.WriteAttributeString ("user", User);
			writer.WriteAttributeString ("timestamp", TimeStamp);
			writer.WriteAttributeString ("visible", Visible? "true": "false");
		}

		public void WriteTags (XmlTextWriter writer)
		{
			if (tags == null)
				return;
			foreach (string k in tags.Keys) {
				writer.WriteStartElement ("tag");
				writer.WriteAttributeString ("k", k);
				writer.WriteAttributeString ("v", tags [k]);
				writer.WriteEndElement ();
			}

		}
	}
}


using System;
using System.Net;
using System.IO;
using System.Web;

namespace OpenStreetMap {

	public class WebMessage {
		CookieContainer cookies;
		Exception error;

/*
		static void PrintStream (Stream s)
		{
			StreamReader tr = new StreamReader (s);
			string line;
			while ((line = tr.ReadLine ()) != null) {
				Console.WriteLine (line);
			}
		}
*/

		public Exception Error {
			get {return error;}
		}

		public void Login (string username, string password)
		{
			cookies = new CookieContainer ();
			/* perform the actual login */
			HttpWebRequest req = WebRequest.Create ("http://www.openstreetmap.org/login") as HttpWebRequest;
			req.CookieContainer = cookies;
			req.ServicePoint.Expect100Continue = false;
			req.Method = "POST";
			req.Timeout = 5000;
			req.ContentType = "application/x-www-form-urlencoded";
			req.Referer = "http://www.openstreetmap.org/login";
			req.AllowAutoRedirect = false;
			string data = HttpUtility.UrlEncode ("user[email]") + "=" + HttpUtility.UrlEncode (username) + "&" 
				+ HttpUtility.UrlEncode ("user[password]") + "=" + HttpUtility.UrlEncode (password) + "&"
				+ HttpUtility.UrlEncode ("commit") + "=" + HttpUtility.UrlEncode ("Login") + "&"
				+ HttpUtility.UrlEncode ("referer") + "=" + HttpUtility.UrlEncode ("/login"); 
			StreamWriter logdata = new StreamWriter (req.GetRequestStream());
			logdata.Write (data);
			logdata.Close ();
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			Stream s = res.GetResponseStream ();
			//PrintStream (s);
			s.Close ();
		}

		int GetUserID (string user)
		{
			int id = -1;
			HttpWebRequest req = WebRequest.Create (new Uri ("http://www.openstreetmap.org/user/" + user)) as HttpWebRequest;
			req.ServicePoint.Expect100Continue = false;
			// cookies are not needed and this allows getting the id also for the current user
			//req.CookieContainer = cookies;
			req.Timeout = 5000;
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			Stream s = res.GetResponseStream ();
			StreamReader tr = new StreamReader (s);
			string line;
			while ((line = tr.ReadLine ()) != null) {
				if (line.IndexOf ("Description") >= 0)
					break;
				int pos = line.IndexOf ("message/new/");
				if (pos >= 0) {
					int quotes = line.IndexOf ('"', pos);
					pos += 12;
					string sid = line.Substring (pos, quotes - pos);
					//Console.WriteLine ("User {0} has id: '{1}'", user, sid);
					id = int.Parse (sid);
					break;
				}
			}
			s.Close ();
			return id;
		}

		public bool Send (string user, string subject, string message)
		{
			int id = GetUserID (user);
			return Send (id, subject, message);
		}

		public bool Send (int user, string subject, string message)
		{
			HttpWebRequest req = WebRequest.Create ("http://www.openstreetmap.org/message/new/" + user) as HttpWebRequest;
			req.CookieContainer = cookies;
			req.ServicePoint.Expect100Continue = false;
			req.Method = "POST";
			req.Timeout = 5000;
			req.ContentType = "application/x-www-form-urlencoded";
			req.Referer = "http://www.openstreetmap.org/";
			req.AllowAutoRedirect = false;
			string data = HttpUtility.UrlEncode ("message[title]") + "=" + HttpUtility.UrlEncode (subject) + "&" 
				+ HttpUtility.UrlEncode ("message[body]") + "=" + HttpUtility.UrlEncode (message) + "&"
				+ HttpUtility.UrlEncode ("commit") + "=" + HttpUtility.UrlEncode ("Send");
			StreamWriter logdata = new StreamWriter (req.GetRequestStream());
			logdata.Write (data);
			logdata.Close ();
			HttpWebResponse res = req.GetResponse () as HttpWebResponse;
			Stream s = res.GetResponseStream ();
			//PrintStream (s);
			s.Close ();
			return true;
		}

	}
}


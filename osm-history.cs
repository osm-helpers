/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using OpenStreetMap;

class OsmHistory {

	static void Usage ()
	{
		Usage_r (1);
	}

	static void Usage_r (int retval)
	{
		Console.WriteLine ("Usage:");
		Console.WriteLine ("\tosm-history node|way|relation id");
		Console.WriteLine ("\tosm-history --help");
		Environment.Exit (retval);
	}

	static string[] GetTags (OsmObject obj)
	{
		ICollection tags = obj.Tags;
		string[] arr = new string [tags.Count];
		tags.CopyTo (arr, 0);
		Array.Sort (arr);
		return arr;
	}

	static void CompareTags (OsmObject a, OsmObject b)
	{
		string[] ta = GetTags (a);
		string[] tb = GetTags (b);
		int i = 0;
		int j = 0;
		for (; i < ta.Length && j < tb.Length; i++, j++) {
			string tag = ta [i];
			int cmp = string.Compare (tag, tb [j], StringComparison.Ordinal);
			if (cmp == 0) {
				if (a [tag] != b [tag])
					Console.WriteLine ("\tTag {0}: {1} -> {2}", tag, a [tag], b [tag]);
				continue;
			}
			if (cmp < 0) {
				Console.WriteLine ("\tTag removed: {0} (was: {1})", tag, a [tag]);
				j--;
			} else {
				Console.WriteLine ("\tTag added: {0}: {1}", tb [j], b [tb [j]]);
				i--;
			}
		}
		while (i < ta.Length) {
			Console.WriteLine ("\tTag removed: {0} (was: {1})", ta [i], a [ta [i]]);
			i++;
		}
		while (j < tb.Length) {
			Console.WriteLine ("\tTag added: {0}: {1}", tb [j], b [tb [j]]);
			j++;
		}
	}

	static void CompareNodes (Node a, Node b)
	{
		if (a.Latitude != b.Latitude)
			Console.WriteLine ("\tLatitude {0} -> {1}", a.Latitude , b.Latitude);
		if (a.Longitude != b.Longitude)
			Console.WriteLine ("\tLongitude {0} -> {1}", a.Longitude , b.Longitude);
	}

	// test:
	// way 23059696 
	// way 23059688
	// way 23060188
	// node node 253003550
	// Not the most compact diff output, but good enough for most osm cases
	static void DiffNodes (long[] an, long[] bn) {
		int adj_pos = 0;
		int pos = 0;
		int bpos = 0;
		int index_out = -2;
		StringBuilder sb = new StringBuilder ();
		for (; pos < an.Length; ++pos, ++adj_pos) {
			int mpos = Array.IndexOf (bn, an [pos], bpos);
			if (mpos == adj_pos) {
				bpos++;
				continue;
			}
			if (mpos < 0) {
				if (index_out != adj_pos - 1) {
					sb.AppendFormat (" {0}:", adj_pos);
					index_out++;
				}
				sb.AppendFormat (" -{0}", an [pos]);
				continue;
			}
			for (; bpos < mpos; bpos++) {
				if (index_out != adj_pos - 1) {
					sb.AppendFormat (" {0}:", adj_pos);
					index_out++;
				}
				sb.AppendFormat (" +{0}", bn [bpos]);
			}
			bpos++;
		}
		for (; bpos < bn.Length; bpos++) {
			if (index_out != adj_pos - 1) {
				sb.AppendFormat (" {0}:", adj_pos);
				index_out++;
			}
			sb.AppendFormat (" +{0}", bn [bpos]);
		}
		if (sb.Length > 0) {
			Console.WriteLine ("\tNodes:{0}", sb.ToString ());
			//Console.WriteLine ("Nodes: {0}", bn.Length);
		}
	}

	static void CompareWays (Way a, Way b)
	{
		long[] an = a.Nodes;
		long[] bn = b.Nodes;
		// simple check for a reverse
		if (an.Length == bn.Length && an.Length > 0) {
			if (an [0] != bn [0] && an [0] == bn [bn.Length - 1]) {
				bool failed = false;
				for (int i = 0; i < an.Length; ++i) {
					if (an [i] != bn [bn.Length - i - 1]) {
						failed = true;
						break;
					}
				}
				if (!failed) {
					Console.WriteLine ("\tNodes reversed.");
					return;
				}
			}
		}
		DiffNodes (an, bn);
		// we don't check the nodes coordinates: it would likely be too expensive
	}

	static int CompareMember (RelationMember ma, RelationMember mb) {
		int cmp = string.Compare (ma.Type, mb.Type, StringComparison.Ordinal);
		if (cmp != 0)
			return cmp;
		long diff = ma.Ref - mb.Ref;
		if (diff < 0)
			return -1;
		if (diff > 0)
			return 1;
		return string.Compare (ma.Role, mb.Role, StringComparison.Ordinal);
	}

	static RelationMember[] GetMembers (Relation obj)
	{
		RelationMember[] members = obj.Members;
		if (members.Length <= 1)
			return members;
		members = members.Clone () as RelationMember [];
		Array.Sort (members, CompareMember);
		return members;
	}

	static string MemberDesc (RelationMember m) {
		return string.Format ("type={0} ref={1} role={2}", m.Type, m.Ref, m.Role);
	}

	static void CompareRelations (Relation a, Relation b)
	{
		RelationMember[] ta = GetMembers (a);
		RelationMember[] tb = GetMembers (b);
		int i = 0;
		int j = 0;
		for (; i < ta.Length && j < tb.Length; i++, j++) {
			int cmp = CompareMember (ta [i], tb [j]);
			if (cmp == 0)
				continue;
			if (cmp < 0) {
				Console.WriteLine ("\tMember removed: {0}", MemberDesc (ta [i]));
				j--;
			} else {
				Console.WriteLine ("\tMember added: {0}", MemberDesc (tb [j]));
				i--;
			}
		}
		while (i < ta.Length) {
			Console.WriteLine ("\tMember removed: {0}", MemberDesc (ta [i]));
			i++;
		}
		while (j < tb.Length) {
			Console.WriteLine ("\tMember added: {0}", MemberDesc (tb [j]));
			j++;
		}
	}

	static void PrintDiff (OsmObject a, OsmObject b)
	{
		// FIXME: make it clear when a way/node is removed
		if (a.Visible != b.Visible)
			Console.WriteLine ("\tVisible = {0}", b.Visible);
		CompareTags (a, b);
		if (a is Node)
			CompareNodes (a as Node, b as Node);
		if (a is Way)
			CompareWays (a as Way, b as Way);
		if (a is Relation)
			CompareRelations (a as Relation, b as Relation);
	}

	static int Main (string[] args)
	{
		string type = null;
		long id = 0;
		int i = 0;
		//bool out_data = false;

		if (args.Length > 0 && args [0] == "--help") {
			Usage_r(0);
		}
		if (args.Length > 0 && args [0] == "--out") {
			//out_data = true;
			i++;
		}
		if (args.Length - i != 2)
			Usage ();
		type = args [i];
		if (type != "node" && type != "way" && type != "relation")
			Usage ();
		try {
			id = long.Parse (args [i + 1]);
		} catch {
			Usage ();
		}
	
		DataBase db = new DataBase ();
		IEnumerable<OsmObject> list;
		try {
			list = db.LoadHistory (type, id);
		} catch (Exception e) {
			Console.WriteLine (e.Message);
			return 1;
		}
		OsmObject last = null;
		int v = 1;
		foreach (OsmObject obj in list) {
			Console.WriteLine ("Version: {0} User: {1}, Time: {2}", v, obj.User, obj.TimeStamp);
			v++;
			if (last != null)
				PrintDiff (last, obj);
			last = obj;
		}
		return 0;
	}
}


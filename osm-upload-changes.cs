/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */

using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Text;
using System.Collections.Generic;
using OpenStreetMap;

class OsmUploadChanges {

	class ChangeSet {
		public OsmObject obj;
		public string tag;
		public string old;
		public string val;
		public ChangeSet (OsmObject obj, string tag, string old, string val)
		{
			this.obj = obj;
			this.tag = tag;
			this.old = old;
			this.val = val;
		}
	}

	static Dictionary<string,List<ChangeSet>> user_changes = new Dictionary<string,List<ChangeSet>> ();

	static WebMessage message = null;
	static bool upload = true;
	static bool notify = false;
	//static bool batch = false;
	static bool haspolicy = false;
	const string policyarg = "--policy=";
	static string policy = "\n";
	static string template =
@"Hello @USER@,
the following changes have been made to your OpenStreetMap contributions to
make them more comforming with the rules of the project and of the country
where the contributions are located.
Please review the changes and try to make your later contributions follow
the same conventions these changes try to enforce.
Typical changes involve capitalization, spelling fixes, extra white space removal.
If you have any questions or if you think these changes are a mistake feel
free to conact me.
The message is automatically sent, but a real person will reply.
Thanks!

@POLICY@

The current check resulted in these changes:
@CHANGES@
";
	static string template_file;
	static DataBase db;

	static string Prompt (string item, bool show)
	{
		Console.Write (item);
		StringBuilder sb = new StringBuilder ();
		while (true) {
			ConsoleKeyInfo key = Console.ReadKey (true);
			char c = key.KeyChar;
			if (c == '\n' || c == '\r')
				break;
			if (key.Key == ConsoleKey.Backspace) {
				if (sb.Length > 0) {
					sb.Remove (sb.Length - 1, 1);
					if (show) {
						Console.CursorLeft--;
						Console.Write (' ');
						Console.CursorLeft--;
					}
				}
				continue;
			}
			if (show)
				Console.Write (c);
			if (c != (char)0)
				sb.Append (c);
		}
		return sb.ToString ();
	}

	static void GetPwd (out string user, out string password)
	{
		string osmauthfile = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData), "osm-helpers/osm-auth");
		try {
			TextReader reader = new StreamReader (osmauthfile);
			user = reader.ReadLine ();
			password = reader.ReadLine ();
			if (user != null && password != null)
				return;
		} catch {
			Console.WriteLine ("Could not read credentials from {0} .", osmauthfile);
		}
		Console.WriteLine ("Openstreetmap credentials");
		user = Prompt ("Username: ", true);
		Console.WriteLine ();
		password = Prompt ("Password: ", false);
		Console.WriteLine ();
		return;
		//Console.WriteLine ("{0}: {1} {2}, {3}", user, password, user.Length, password.Length);
		/*
		Environment.Exit (2);
		Console.WriteLine ("Add the file ~/.config/osm-helpers/osm-auth with your username and password, one per line.");
		user = password = null;
		Environment.Exit (2);
		*/
	}

	static void Usage ()
	{
		Usage_r (1);
	}

	static void Usage_r (int retval)
	{
		Console.WriteLine ("Usage: osm-upload-changes [OPTIONS] file");
		Console.WriteLine ("       osm-upload-changes --help");
		Console.WriteLine ("Options:");
		Console.WriteLine ("\t--help           Show this help message");
		Console.WriteLine ("\t--no-upload      Do everything except the actual upload");
		Console.WriteLine ("\t--notify[=TEMPL] Notify the user of the changes, using TEMPL for the message");
		Console.WriteLine ("\t--policy=POL     Notify the user of the changes, setting the change policy to POL");
		//Console.WriteLine ("\t--failed file   Output in file the entries that failed");
		Environment.Exit (retval);
	}

	static void Upload (OsmObject obj, string user, string password, ref int skipped, ref int uploaded)
	{
		if (upload) {
			try {
				db.UpLoad (obj, user, password);
			} catch {
				Console.Write ('U');
				skipped++;
				return;
			}
		}
		Console.Write ('.');
		uploaded++;
	}

	static void AddChangeSet (string user, OsmObject obj, string tag, string old, string val)
	{
		List<ChangeSet> list = null;
		if (!user_changes.TryGetValue (user, out list)) {
			list = new List<ChangeSet> ();
			user_changes [user] = list;
		}
		list.Add (new ChangeSet (obj, tag, old, val));
	}

	static void PrepareNotification (string user, string password)
	{
		if (!notify)
			return;
		if (template_file != null) {
			StreamReader sr = new StreamReader (template_file);
			string new_tmpl = sr.ReadToEnd ();
			template = new_tmpl;
		}
		message = new WebMessage ();
		message.Login (user, password);
	}

	static void SendNotifications ()
	{
		if (!notify)
			return;
		foreach (string user in user_changes.Keys) {
			string msg = template.Replace ("@USER@", user);
			List<ChangeSet> list = user_changes [user];
			StringBuilder sb = new StringBuilder ();
			foreach (ChangeSet c in list) {
				if (c.val == "")
					sb.AppendFormat ("{0} {1}: tag {2} removed\n",
						c.obj.ObjectType, c.obj.ID, c.tag);
				else
					sb.AppendFormat ("{0} {1}: tag {2} changed '{3}' to '{4}'\n",
						c.obj.ObjectType, c.obj.ID, c.tag, c.old, c.val);
			}
			msg = msg.Replace ("@CHANGES@", sb.ToString ());
			//Console.WriteLine (msg);
			if (haspolicy)
				msg = msg.Replace ("@POLICY@", policy);
			else
				msg = msg.Replace ("@POLICY@", "");
			if (upload) {
				message.Send (user, "osm-check update notification", msg);
				Console.WriteLine ("Notification sent to {0}.", user);
			}
		}
	}

	static int Main (string[] args)
	{
		int uploaded = 0;
		int skipped = 0;
		string user, password;
		string file = null;
		//string failed_file = null;

		for (int i = 0; i < args.Length; ++i) {
			if (args [i] == "--help") {
				Usage_r(0);
			} else if (args [i] == "--no-upload") {
				upload = false;
			} else if (args [i] == "--failed") {
				if (++i >= args.Length)
					Usage ();
				//failed_file = args [i];
			} else if (args [i] == "--notify") {
				notify = true;
			} else if (args [i].StartsWith ("--notify=")) {
				if (args [i].Length == 9)
					Usage ();
				template_file = args [i].Substring (9);
				notify = true;
			} else if (args [i].StartsWith (policyarg)) {
				if (args [i].Length == policyarg.Length)
					Usage ();
				policy = args [i].Substring (policyarg.Length);
				haspolicy = true;
				notify = true;
			} else {
				if (args.Length - i == 1) {
					file = args [i];
				} else {
					Usage ();
				}
			}
		}
		GetPwd (out user, out password);
		PrepareNotification (user, password);
		db = new DataBase ();
		XmlTextReader reader = new XmlTextReader (file);
		//bool first_element = false;
		OsmObject last_obj = null;
		while (reader.Read ()) {
			if (reader.NodeType == XmlNodeType.Element) {
				string name = reader.Name;
				string tag = null;
				string old = null;
				string newv = null;
				string culprit = null;
				long id = -1;
				OsmObject obj = null;
				if (name == "osmchanges") {
					//first_element = true;
					continue;
				}
				for (int i = 0; i < reader.AttributeCount; ++i) {
					reader.MoveToAttribute (i);
					if (reader.Name == "id")
						id = long.Parse (reader.Value);
					else if (reader.Name == "tag")
						tag = reader.Value;
					else if (reader.Name == "old")
						old = reader.Value;
					else if (reader.Name == "new")
						newv = reader.Value;
					else if (reader.Name == "user")
						culprit = reader.Value;
					else
						throw new Exception ("Invalid attribute: " + reader.Name);
				}
				if (last_obj != null && last_obj.ID == id && last_obj.ObjectType == name) {
					obj = last_obj;
				} else {
					// we didn't upload yet: do it now
					if (last_obj != null) {
						Upload (last_obj, user, password, ref skipped, ref uploaded);
						last_obj = null;
					}

					try {
						obj = db.LoadObject (name, id);
					} catch (Exception e) {
						Console.WriteLine ("{0}: {1}", e, e.Message);
						//Console.Write ('E');
						skipped++;
						continue;
					}
				}
				if (id == -1 || tag == null || old == null || newv == null)
					throw new Exception ("Invalid element: " + name);

				if (obj [tag] != old) {
					Console.Write ('*');
					skipped++;
					continue;
				}
				// we automatically remove tags with value == ""
				if (newv == "")
					obj.RemoveTag (tag);
				else
					obj [tag] = newv;
				last_obj = obj;
				if (notify && culprit != null) {
					AddChangeSet (culprit, obj, tag, old, newv);
				}
			}
		}
		// we didn't upload yet: do it now
		if (last_obj != null)
			Upload (last_obj, user, password, ref skipped, ref uploaded);
		Console.WriteLine ();
		Console.WriteLine ("Uploaded: {0}, Skipped: {1}", uploaded, skipped);
		SendNotifications ();
		return 0;
	}
}


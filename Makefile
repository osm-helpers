PREFIX=/usr/local
GMCS=gmcs
VERSION=0.3

scripts=osm-upload-changes osm-check osm-history
files=osm-check.exe osm-upload-changes.exe osm-history.exe OpenStreetMap.dll \
	$(scripts)

all: $(files)

OpenStreetMap.dll: Node.cs OsmObject.cs Way.cs Relation.cs DataBase.cs RelationMember.cs WebMessage.cs
	$(GMCS) -debug -out:OpenStreetMap.dll -target:library -r:System.Web $^

osm-upload-changes.exe: osm-upload-changes.cs OpenStreetMap.dll
	$(GMCS) -debug -r:OpenStreetMap.dll osm-upload-changes.cs

osm-check.exe: osm-check.cs OpenStreetMap.dll
	$(GMCS) -debug -r:OpenStreetMap.dll osm-check.cs

osm-history.exe: osm-history.cs OpenStreetMap.dll
	$(GMCS) -debug -r:OpenStreetMap.dll osm-history.cs

tar: all cleantar
	mkdir .tar
	cp * .tar
	mv .tar osm-helpers-$(VERSION)
	tar zcvf osm-helpers-$(VERSION).tar.gz osm-helpers-$(VERSION)
	rm -rf osm-helpers-$(VERSION)
	mv osm-helpers-$(VERSION).tar.gz ..

cleantar:
	rm -f $(scripts) *.mdb

clean:
	rm -f *.mdb $(files)

osm-check: osm-check.in
	@echo "Using prefix: $(PREFIX)"
	@sed "s,@PREFIX@,$(PREFIX)," $^ > $@
	chmod 755 $@

osm-history: osm-history.in
	@echo "Using prefix: $(PREFIX)"
	@sed "s,@PREFIX@,$(PREFIX)," $^ > $@
	chmod 755 $@

osm-upload-changes: osm-upload-changes.in
	@echo "Using prefix: $(PREFIX)"
	@sed "s,@PREFIX@,$(PREFIX)," $^ > $@
	chmod 755 $@

install: all
	mkdir -p $(PREFIX)/lib/osm-helpers
	install OpenStreetMap.dll $(PREFIX)/lib/osm-helpers
	install osm-upload-changes.exe $(PREFIX)/lib/osm-helpers
	install osm-check.exe $(PREFIX)/lib/osm-helpers
	install osm-history.exe $(PREFIX)/lib/osm-helpers
	mkdir -p $(PREFIX)/bin
	install osm-upload-changes $(PREFIX)/bin
	install osm-check $(PREFIX)/bin
	install osm-history $(PREFIX)/bin

.PHONY: clean cleantar install tar all

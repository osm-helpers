/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.Xml;
using System.Collections.Specialized;

namespace OpenStreetMap {

	public class Way: OsmObject {
		long[] nodes;

		public override string ObjectType {
			get {
				return "way";
			}
		}

		public long[] Nodes {
			get {
				if (nodes == null)
					return new long [0];
				return nodes;
			}
			set {
				if (value == null)
					throw new ArgumentNullException ("value");
				nodes = value.Clone () as long [];
				Changed = true;
			}
		}

		public override void WriteXml (XmlTextWriter writer)
		{
			writer.WriteStartElement ("way");
			WriteCommonAttrs (writer);
			for (int i = 0; i < nodes.Length; ++i) {
				writer.WriteStartElement ("nd");
				writer.WriteAttributeString ("ref", nodes [i].ToString ());
				writer.WriteEndElement ();
			}
			WriteTags (writer);
			writer.WriteEndElement ();
		}

	}
}


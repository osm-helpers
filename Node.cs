/*
 * Author: Paolo Molaro
 * Copyright (c) 2008 Paolo Molaro lupus@oddwiz.org
 * License: MIT/X11, see the MIT.X11 file.
 */
using System;
using System.Xml;
using System.Collections.Specialized;

namespace OpenStreetMap {

	public class Node: OsmObject {
		double lat, lon;

		public override string ObjectType {
			get {
				return "node";
			}
		}

		public double Latitude {
			get {
				return lat;
			}
			set {
				lat = value;
				Changed = true;
			}
		}

		public double Longitude {
			get {
				return lon;
			}
			set {
				lon = value;
				Changed = true;
			}
		}

		public override void WriteXml (XmlTextWriter writer)
		{
			writer.WriteStartElement ("node");
			WriteCommonAttrs (writer);
			writer.WriteAttributeString ("lat", lat.ToString ());
			writer.WriteAttributeString ("lon", lon.ToString ());
			WriteTags (writer);
			writer.WriteEndElement ();
		}
	}
}

